//
//  KMPage.h
//  Kimono
//
//  Created by James Dumay on 8/04/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KMBrowserTab <NSObject>

- (NSString*)title;
- (NSImage*)icon;
- (NSURL*)location;
- (double)progress;
- (BOOL)loading;

@end
