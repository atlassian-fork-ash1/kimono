# Kimono

A small light web browser for Mac OS X based on webkit

## Building

* Get Xcode from the App Store. Its deliciously free.
* Clone this repository
* Open the `Kimono.xcodeproj` in Xcode
* Click the nice big run button at the top left hand side of the Xcode window
* Wait a few seconds
* You now have Kimono running

## Contributing

There are lots of things todo. Play around and see what needs to be implemented.

Heres a quick shortlist

* Some sort of application icon. Would be great to have something cute.
* Better icons. I was using the wonderful Glyphish Pro icon set, but I had to strip it from the repository to open source it
* Open in new window/open in new tab
* Tab switcher needs some keyboard bindings love
* url bar is pretty dumb. Could use magic like autocomplete, search, etc
* Save pages to the file system
* Load pages from the file system
* Slide toolbar over webview when mouse moves

Oh and don't forget to checkout the `LICENSE` file in the source code. All contributions are re-licensed under it to avoid dark armies  of lawyers battling on our plain of existence. We prefer to keep them in lawyer-mordor.

## First time rocking out with Webkit?

Read up on the [Webkit Objective-C programming guide](https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/DisplayWebContent/DisplayWebContent.html) to get a good introduction into the Webkit Cocoa architecture. I sleep with mine under my pillow at all times.

## Screenshots

![Screenshot](https://bitbucket.org/atlassian/kimono/downloads/screenshot.png)
